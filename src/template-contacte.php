<?php get_header(); /*
 * Template Name: Contact hub
 * Template Post Type: espectacles, page
 */?>

	<main role="main" aria-label="Content" class="uk-container main" >
		
		<section class="uk-section" >
			
			<div class="uk-child-width-1-4" uk-grid>


			  	<div class="uk-text-center">
			      <a href="mailto:atriumviladecans@atriumviladecans.com" target="_blank" uk-tooltip="atriumviladecans@atriumviladecans.com">
			        <img  src="<?php echo esc_url(get_template_directory_uri()) . '/img/xarxes/contacte.png'; ?>" alt="Contacte">

			      </a>
			      <p><?php pll_e('Contacte') ?></p>
			      
			    </div>

			    <div class="uk-text-center">
			      <a href="mailto:taquilla@atriumviladecans.com" target="_blank" uk-tooltip="taquilla@atriumviladecans.com">
			        <img  src="<?php echo esc_url(get_template_directory_uri()) . '/img/xarxes/taquilla.png'; ?>" alt="Contacte">

			      </a>
			      <p><?php pll_e('Contacte Taquilles') ?></p>
			   
			    </div>

			     <div class="uk-text-center">
				      <a href="telf:0034936594160" target="_blank">
				        <img  src="<?php echo esc_url(get_template_directory_uri()) . '/img/xarxes/phone.png'; ?>" alt="Trucar ">
				      </a>
				      <p>+34 93 659 41 60</p>
				  </div>

			    <div class="uk-text-center">
				      <a href="https://goo.gl/maps/5NoMBMTXX9U2" target="_blank">
				        <img  src="<?php echo esc_url(get_template_directory_uri()) . '/img/xarxes/google-maps.png'; ?>" alt="Youtube ">
				      </a>
				      <p><?php pll_e('Com arribar') ?></p>
				 </div>

			 </div>

			<div  class="uk-flex-center uk-child-width-1-4" uk-grid>
				<div class="uk-text-center">
			      <a href="https://twitter.com/AtriumTeatre" target="_blank">
			        <img  src="<?php echo esc_url(get_template_directory_uri()) . '/img/xarxes/twitter.png'; ?>" alt="Contacte">
			      </a>
			      <p>Twitter</p>
			    </div>
    
				  <div class="uk-text-center">
				      <a href="https://www.facebook.com/AtriumViladecans" target="_blank">
				        <img  src="<?php echo esc_url(get_template_directory_uri()) . '/img/xarxes/facebook.png'; ?>" alt="facebook">
				      </a>

				      <p>Facebook</p>
				    </div>

					<div class="uk-text-center">
				      <a href="https://www.instagram.com/atriumviladecans" target="_blank">
				        <img  src="<?php echo esc_url(get_template_directory_uri()) . '/img/xarxes/instagram.svg'; ?>" alt="Instagram">
				      </a>

				      <p>Instagram</p>
				    </div>


			</div>
				  <?php edit_post_link(); ?>

		</section>
		
	</main>



<?php get_footer(); ?>
