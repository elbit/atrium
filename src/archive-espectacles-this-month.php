<?php get_header(); /*
 * Template Name: archive this month
 * Template Post Type: espectacles, page
 */?>

	<main role="main" aria-label="Content" class="uk-container main">
	<h1><?php pll_e('Aquest mes:'); ?> <span class="uk-text-bold"><?php echo date_i18n("F"); ?></span> </h1>
       <section class="uk-section-xsmall flt-groups uk-container">
	   		
            <?php get_template_part( '/parts/programacio/filtres/filtre-categories_menu' ); ?>
        	<?php //get_template_part( 'loop-calendar-filter' ); ?>
        </section>

        <section class="uk-section-small">
			<?php get_template_part( '/parts/programacio/programacio-this-month' ); ?>
        </section>

        <section class="uk-section-small ">
            <?php //get_template_part( 'loop-fidelitzacio-home' ); ?>
        </section>
		
	</main>

    <?php get_template_part( '/parts/programacio/filtres/isotope-js' ); ?>



<?php get_footer(); ?>

