<?php get_header(); ?>

	<main role="main" aria-label="Content" class="uk-container uk-container-small" >
	
		<!-- section -->
		<section <?php post_class('uk-article article'); ?>>

			<h1><?php esc_html_e( 'Category: ', 'html5blank' ); single_cat_title(); ?></h1>

			<?php get_template_part( 'loop' ); ?>

			<?php get_template_part( 'pagination' ); ?>

		</section>
		<!-- /section -->
	</main>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
