<?php get_header(); /*
 * Template Name: serveis
 * Template Post Type: espectacles, page
 */?>

	<main role="main" aria-label="Content" class="uk-container uk-container-small uk-article" style="min-height: 75vh;">
		
		<h1><?php the_title(); ?></h1>

		<section class="">

			<h2>Serveis</h2>
			
			<p>Atrium Viladecans vol fer única l’experiència dels qui visiten el seu teatre, és per això que ofereix els següents serveis als seus usuaris:</p>
			
			<ul class="uk-width-1-1 bit-serveis-list" uk-grid>
			 		
			 		<li class="" id="acollida">
			 			<img class="" src="<?php echo esc_url(get_template_directory_uri()) . '/img/serveis/acollida.png'?>" alt="" />
			 		<p>
			 		
			 		<b>Servei d’acollida per a nenes i nens de 3 a 11 anys.</b> 
			 				Disponible en els espectacles que mostrin la icona corresponent. Cal sol·licitar-ho amb un mínim de 5 dies d’antelació al mail acollida@atriumviladecans.com. 
							 <br><strong class="uk-text-small uk-font-underline" style="text-decoration:underline"> Servei temporalment fora de servei seguint indicacions sanitàries per la Covid-19</strong>
					</p>
			 		</li>
				
				
			 	
			 	<li id="accessibilitat"><img class=" wp-image-187" src="<?php echo esc_url(get_template_directory_uri()) . '/img/serveis/discapacitats.png'?>" alt="" /><p><b>Accessos habilitats per a persones amb mobilitat reduïda</b></p></li>
			 	
			 	<li id="apropa"><img class=" wp-image-181" src="<?php echo esc_url(get_template_directory_uri()) . '/img/serveis/apropa.png'?>" alt="" /><p><b>Apropa Cultura:</b> Preus especials per a centres socials que treballen amb persones amb vulnerabilitat social o discapacitat a través del programa Apropa Cultura.</p></li>
			 	
			 	<li id="parking"><img class=" wp-image-181" src="<?php echo esc_url(get_template_directory_uri()) . '/img/serveis/parking.png'?>" alt="" /><p><b>1h d’aparcament gratuït</b> al Pàrquing del Torrent Ballester
			C/ Mare de Déu de Sales, 69. Cal demanar el tiquet a la taquilla del teatre.</p></li>
			 	
			 	<!-- <li id="bar"><img class=" wp-image-181" src="<?php echo esc_url(get_template_directory_uri()) . '/img/serveis/bar.png'?>" alt="" /> <p><b>Servei de bar i restaurant</b> tots els dies d’espectacle</p></li>  -->

				<li id="bar"><img class=" wp-image-180" src="<?php echo esc_url(get_template_directory_uri()) . '/img/serveis/bar.png'?>" alt=""/><p><strong>Servei de bar i restaurant</strong> tot els dies d’espectacle <br><strong class="uk-text-small uk-font-underline" style="text-decoration:underline"> Servei temporalment fora de servei.</strong></p></li>
			 	
			 	<li id="guarda-roba"><img class=" wp-image-180" src="<?php echo esc_url(get_template_directory_uri()) . '/img/serveis/guarda-roba.png'?>" alt=""/><p><strong>Guarda-roba</strong><br> <strong class="uk-text-small uk-font-underline" style="text-decoration:underline"> Servei temporalment fora de servei seguint indicacions sanitàries per la Covid-19</strong></p></li>
			 	
			 	<li id="acollida"><img class=" wp-image-181" src="<?php echo esc_url(get_template_directory_uri()) . '/img/serveis/alzador.png'?>" alt="" /><p><b>Alçadors de butaca</b> per a espectacles familiars</p></li>
			
			</ul>


		<p>  Comptem, a més, d’una llarga tradició d’activitats paral·leles: masterclass, taules rodones amb els artistes, copa de vi al final de l’espectacle, Photo-Call amb els actors… Clica a sobre de la imatge i descarrega’t les properes activitats d’aquesta temporada! <br>
		<strong class="uk-text-small uk-font-underline" style="text-decoration:underline">Servei temporalment fora de servei seguint indicacions sanitàries per la Covid-19</strong></p>
				 


		</section>

		<section>
			<?php if ( have_posts()) : while ( have_posts() ) : the_post(); ?>
			
			<?php the_content(); ?>


		<?php endwhile; ?>

		<?php else : ?>

			<!-- article -->
			<article>

				<h2><?php esc_html_e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

			</article>
			<!-- /article -->

		<?php endif; ?>
		</section>
		
	</main>

<?php edit_post_link(); ?>

<?php get_footer(); ?>
