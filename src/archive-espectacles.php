<?php get_header(); ?>

	<main role="main" aria-label="Content" class="uk-flt main">

       <section class="flt-groups uk-container uk-margin-top">
	   		
            <?php get_template_part( '/parts/programacio/filtres/filtre-categories_menu' ); ?>
        	<?php get_template_part( '/parts/programacio/filtres/filtre-calendari_menu' ); ?>
			<?php get_template_part( '/parts/programacio/filtres/filtre-categories-home_menu' ); ?>
					
        </section>

        <?php echo do_shortcode( '[searchandfilter headings="Select categories:" types="checkbox" fields="category"]' ); ?>

        <section class="uk-section-xsmall uk-margin-bottom-remove flt-m" >
            
            <?php get_template_part( '/parts/programacio/programacio-general' ); ?>
        
        </section>

    </main>

<?php get_template_part( '/parts/programacio/filtres/isotope-js' ); ?>

<?php get_footer(); ?>






