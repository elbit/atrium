<?php get_header(); ?>

	<main role="main" aria-label="Content" class="main front-page-main">
		
		<section class="uk-section-xxsmall claim">
            <?php get_template_part( '/parts/front-page/claim' ); ?>
        </section>

        <section  class="uk-section-small dropdown-nav-home">
            <?php get_template_part( '/parts/front-page/big-dropdown' ); ?>
        </section>

        <section class="uk-section-small espectacles-destacats">
            <?php get_template_part( '/parts/front-page/espectacles-destacats' ); ?>
        </section>

        <section class="uk-section fidelitzacio">
            <?php get_template_part( '/parts/front-page/fidelitzacio' ); ?>
        </section>

        <section class="uk-section-xsmall filtre-agenda ">
            <?php get_template_part( '/parts/front-page/filtres/filtre-agenda' ); ?>
        </section>

        <section class="uk-section-small categories-home">
            <?php get_template_part( '/parts/front-page/filtres/filtre-categories' ); ?>
        </section>
        
        <section class="uk-section-small serveis">
            <?php get_template_part( '/parts/front-page/serveis' ); ?>
        </section>

        <section class="uk-section  xarxes">
            <?php get_template_part( '/parts/front-page/xarxes' ); ?>
        </section>
		
	</main>

<?php get_footer(); ?>

