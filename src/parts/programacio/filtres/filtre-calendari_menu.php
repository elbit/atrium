<?php 
	$today = date('Ymd');
    $args  = array(
		'post_type' => 'espectacles', 
		'posts_per_page' => -1,
        'order'=> 'ASC',
        'orderby' => 'meta_value',
		'meta_key' => 'data',
        'meta_query'     => array(
            array(
				'key'     => 'data',
				'compare' => '>=',
                'value'   => $today,
			),
            
        ),

    );
    $loop = new WP_Query( $args);
  
?>

<style type="text/css">
    .no-fouc {display:none;}
</style>

<script type="text/javascript">
    document.documentElement.className = 'no-fouc';
    $(window).on("load", function() {
    $('.no-fouc').removeClass('no-fouc');
    });    
</script>

<div class="uk-margin-top uk-grid-collapse bit-filter" uk-grid>
    
    <h3 class="uk-width-1-6 bit-filter-title"><?php pll_e('Vull venir:') ?></h3>

    <div data-filter-group="flt-calendar" class="uk-button-group flt-button-group_calendar bit-filter-group">
        <a data-filter="*" class="uk-button uk-button-default uk-button-small selector is-checked bit-text-xsmall bit-filter-item"><?php pll_e('Tots') ?></a>
        
    <?php while ( $loop->have_posts() ) : $loop->the_post();  $date = new DateTime(get_field('data'));
    $mes = "F";
        $dia = "l";
        $dianum = "j";
        $any = "Y";
        $unixtimestamp = strtotime(get_field('data')); ?>
    
    <a data-filter=".<?php echo date_i18n($mes, $unixtimestamp); ?>" class="uk-button uk-button-default uk-button-small selector bit-text-xsmall">
            <?php echo date_i18n($mes, $unixtimestamp); ?>
        </a>
   
   <?php endwhile; wp_reset_query(); ?>
    </div>
</div>

<script>
    var seen = {};
    $('.selector').each(function() {
        var txt = $(this).text();
        if (seen[txt])
            $(this).remove();
        else
            seen[txt] = true;
    });
</script>
























