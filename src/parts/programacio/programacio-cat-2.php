<?php //https://tomoro.com.au/blog/sort-and-group-posts-by-custom-field-with-unique-headings-in-wordpress/
    
    //$date = new DateTime(get_field('data'));
    //$mes = $date->format('F');
    $today = date("Y-m-d");
    $actualMonth = date(" Y-m-d", strtotime('first day of +1 month'));
    $nextMonth = date(" Y-m-d", strtotime('first day of +2 month'));
    $nextWeek = date(" Y-m-d", strtotime('next monday '));
    $thisWeek = date(" Y-m-d", strtotime('this week'));
   
    // $date = get_field("data");
    // $dateTime = DateTime::createFromFormat("Ymd", $date);
    // $month = $dateTime->format('m');
   
    $today = date('Ymd');//comentar x inabilitar
    $args = array(
        'post_type' => 'espectacles',
        'category_name' => '',
        //'taxonomy' => 'categories_home',
        'tax_query' => array( 
        array( 
            'taxonomy' => 'categories_home', //or tag or custom taxonomy
            'field' => 'slug', 
            //'terms' => array('81'),
            'terms' => array('magrada-el-teatre','me-gusta-el-teatro'),

        ) 
    ) ,
        'posts_per_page' => -1,
        'order' => 'ASC',
        'orderby' => 'meta_value',
        'meta_key' => 'data',
        'meta_query'     => array(
            array(
                'key'     => 'data',
                'compare' => '>=',
                'value'   => $today,
            ),
            
        ),
    );
	$branch_posts = get_posts($args); 
	foreach($branch_posts as $post) : 
	setup_postdata( $post ); 
						
?>

<?php //
        // Only show posts with the 'State' custom field
        if (get_field('data')) {
        
            // Set the state of this post
            $unixtimestamp = strtotime(get_field('data'));
            $date = new DateTime(get_field('data'));
            
            $classes = "uk-card uk-card-default uk-margin-medium-bottom espectacle-destacat flt-item";
            $classesH = "uk-margin-small-top uk-margin-bottom espectacle-destacat flt-item-m ";
            $classesW = "uk-container programacio-month";
            $mes = "F";
            $dia = "l";
            $dianum = "j";
            $any = "Y";
            $unixtimestamp = strtotime(get_field('data'));
            $mes = date_i18n($mes, $unixtimestamp);
            $any = date_i18n($any, $unixtimestamp);

            
            
            $current_state =  $mes;

            // If this post doesn't have the same 'State' as the last one, let's give this one a heading and open the <ul>
            if ($current_state != $previous_state) { 
                
            // $previous_state isn't set until the end of this foreach, so the first one doesn't have this variable
            // If this isn't the first list, close the last list's <ul>
            if ($previous_state) { echo '</div> <!-- end cards wrapper cond--> <hr class="flt-item-m '; echo $mes;  echo ' "/></div><!-- end container month cond-->' ; }
        ?>

<div <?php post_class("$classesW $mes");?> >
    
    <div class="month-header ">
        <h2 class="<?php echo $classesH; echo $mes; ?>" ><strong><?php echo $current_state, ' ',$any; ?></strong></h2>
    </div>
        
    <div  class="flt-items flt uk-margin-bottom uk-child-width-1-4 bit-grid-1-4" >
        
    <?php } ?>

        <div <?php post_class("$classes $mes");?> ">
            
            <div class="uk-card-body uk-padding-remove" >

          <span class="uk-text-small uk-position-top-right" style="color:white;background-color: #333;padding:2px 8px; font-weight: 300;"> 
            <?php exclude_post_categories('Destacat,Destacado',' | '); ?> 

        </span>



                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                    <?php the_post_thumbnail( 'programacio' );?>
                </a>
            </div>
            
            <div class="uk-card-footer uk-padding-small uk-flex uk-flex-center">
                
                <a class="uk-button uk-button-secondary uk-button-small uk-button-vw uk-width-1-2" href="<?php the_permalink(); ?>">+ info</a>
                
                <a class="uk-button uk-button-primary uk-button-small uk-button-vw uk-width-1-2 event-cta-compra-espectacle-programacio" href=" <?php the_field('enllas_koobin'); ?>" target="_blank">Comprar</a>
            
            </div>
            
            <div class="uk-card-footer uk-padding-remove-horizontal uk-text-center">
                    

            <?php 
                $mes = "M";
                $dia = "D";
                $dianum = "j";
                $any = "Y";
                $unixtimestamp = strtotime(get_field('data'));
                $unixtimestamp2 = strtotime(get_field('data_2'));
            ?>

                <h4 class="uk-margin-small-bottom "><?php the_title('')?></h4>
                
                <h5 class="uk-margin-remove-top uk-margin-small-bottom uk-button uk-button-default ">
                     <span class="uk-icon-sub" uk-icon="icon: calendar"></span>
                     <?php echo date_i18n($dia, $unixtimestamp); ?>
                    <?php echo date_i18n($dianum, $unixtimestamp); ?>
                    <?php echo date_i18n($mes, $unixtimestamp); ?>
                    - <?php the_field('hora')?>
                </h5><br>
                 <?php if( get_field('data_2') ):  ?>
                 <h5 class="uk-margin-remove-top uk-margin-small-bottom uk-button uk-button-default ">
                     <span class="uk-icon-sub" uk-icon="icon: calendar"></span>
                     <?php echo date_i18n($dia, $unixtimestamp2); ?>
                    <?php echo date_i18n($dianum, $unixtimestamp2); ?>
                    <?php echo date_i18n($mes, $unixtimestamp2); ?>
                    - <?php the_field('hora_2')?>
                </h5><br>
                 <?php endif; ?> 
                
                <span class="uk-text-small">

                    <?php if( get_field('preu_general') ):  ?>
                        <?php pll_e('General') ?>: <?php the_field('preu_general'); ?> -
                    <?php endif; ?>  
                    
                    <?php if( get_field('preu_general_indef') ):  ?>
                        <?php pll_e('General') ?>: <?php the_field('preu_general_indef'); ?> 
                    <?php endif; ?>  
                    
                    <?php if( get_field('preu_amic') ):  ?>
                        <?php pll_e('Amic') ?>: <?php the_field('preu_amic'); ?> 
                    <?php endif; ?>
                    
                    <?php if( get_field('preu_amic_indef') ):  ?><br>
                        <?php pll_e('Amic') ?>: <?php the_field('preu_amic_indef'); ?> 
                    <?php endif; ?>
                </span>
                    
            </div>

        </div>

            <?php 
        
            $unixtimestamp = strtotime(get_field('data'));
            
            $mes = "F";
            $unixtimestamp = strtotime(get_field('data'));
            $mes = date_i18n($mes, $unixtimestamp);
            // Remember, we closed the <ul> before the <li>'s, clever huh?
            
            // Set the 'State' of the this post, to compare to the next post
            $previous_state = $mes;
           
           } ?>

<?php endforeach; wp_reset_postdata(); ?>

</div> 



