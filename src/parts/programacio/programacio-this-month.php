<?php //https://tomoro.com.au/blog/sort-and-group-posts-by-custom-field-with-unique-headings-in-wordpress/

    //$date = new DateTime(get_field('data'));
    //$mes = $date->format('F');
    $today = date("Y-m-d");
    $actualMonth = date(" Y-m-d", strtotime('first day of +1 month'));
    $nextMonth = date(" Y-m-d", strtotime('first day of +2 month'));
    $nextWeek = date(" Y-m-d", strtotime("next monday +7 days"));
    $thisWeek = date(" Y-m-d", strtotime("next monday"));
   
    // $date = get_field("data");
    // $dateTime = DateTime::createFromFormat("Ymd", $date);
    // $month = $dateTime->format('m');
   
    $args = array(
		'post_type' => 'espectacles',
		'posts_per_page' => -1,
		'order' => 'ASC',
		'orderby' => 'meta_value',
        'meta_key' => 'data',
        'meta_query'     => array(
            array(
				'key'     => 'data',
				'compare' => '>=',
                'value'   =>  $today,
                'type' => 'DATE'
            ),
            array(
                'key' => 'data',
                'compare' => '<=',
                'value' => $actualMonth ,
                'type' => 'DATE'
               
                            )
            
        ),
	);

	$branch_posts = get_posts($args); 
	foreach($branch_posts as $post) : 
	setup_postdata( $post ); 
						
?>

<?php //
		// Only show posts with the 'State' custom field
		if (get_field('data')) {
		
			// Set the state of this post
            $unixtimestamp = strtotime(get_field('data'));
            $date = new DateTime(get_field('data'));
            $classes = "uk-card uk-width-1-4 uk-card-default espectacle-destacat flt-item";
            $mes = "F";
            $dia = "l";
            $dianum = "j";
            $any = "Y";
            $unixtimestamp = strtotime(get_field('data'));
            $mes = date_i18n($mes, $unixtimestamp);
           
            $current_state =  $mes;

			// If this post doesn't have the same 'State' as the last one, let's give this one a heading and open the <ul>
			if ($current_state != $previous_state) { 
				
			// $previous_state isn't set until the end of this foreach, so the first one doesn't have this variable
			// If this isn't the first list, close the last list's <ul>
			if ($previous_state) { echo '</div></div>'; }
		?>
			<div class="uk-container hola">

                <h2 class="no-results" style="display: none"> nope</h2>
            
                <div  class="uk-child-width-1-4 uk-grid-medium uk-grid-match flt" uk-grid>
		<?php } ?>
       
            
            
            <?php get_template_part( '/parts/programacio/programacio-card' ); ?>
     

					
        <?php 
        
            $unixtimestamp = strtotime(get_field('data'));
            
            $mes = "F";
            $unixtimestamp = strtotime(get_field('data'));
            $mes = date_i18n($mes, $unixtimestamp);
			// Remember, we closed the <ul> before the <li>'s, clever huh?
			
			// Set the 'State' of the this post, to compare to the next post
			$previous_state = $mes;
	} ?>
<?php endforeach; wp_reset_postdata(); ?>

 
   
</div>



