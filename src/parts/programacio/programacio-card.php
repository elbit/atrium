

<div  
<?php  
$mes = "F"; 
$mes = date_i18n($mes, $unixtimestamp);
$classes = "flt-item flt-item-m"; post_class("$classes $mes"); 

?> 
><!-- //for grid column -->
    
    <div <?php $classes = "uk-card uk-card-default uk-margin-medium-bottom espectacle-destacat "; post_class("$classes $mes");?> ">
            
            <div class="uk-card-body uk-padding-remove" >

          <span class="uk-text-small uk-position-top-right" style="color:white;background-color: #333;padding:2px 8px; font-weight: 300;"> 
            <?php exclude_post_categories('Destacat,Destacado',' | '); ?> 

        </span>



                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                    <?php the_post_thumbnail( 'programacio' );?>
                </a>
            </div>
            
            <div class="uk-card-footer uk-padding-small uk-flex uk-flex-center">
                
                <a class="uk-button uk-button-secondary uk-button-small uk-button-vw uk-width-1-2" href="<?php the_permalink(); ?>">+ info</a>
                
                <a class="uk-button uk-button-primary uk-button-small uk-button-vw uk-width-1-2 event-cta-compra-espectacle-programacio" href=" <?php the_field('enllas_koobin'); ?>" target="_blank">Comprar</a>
            
            </div>
            
            <div class="uk-card-footer uk-padding-remove-horizontal uk-text-center">
                    

            <?php 
                $mes = "M";
                $dia = "D";
                $dianum = "j";
                $any = "Y";
                $unixtimestamp = strtotime(get_field('data'));
                $unixtimestamp2 = strtotime(get_field('data_2'));
                $unixtimestamp3 = strtotime(get_field('data_3'));
            ?>

                <h4 class="uk-margin-small-bottom "><?php the_title('')?></h4>
                
                <h5 class="uk-margin-remove-top uk-margin-small-bottom uk-button uk-button-default ">
                     <span class="uk-icon-sub" uk-icon="icon: calendar"></span>
                     <?php echo date_i18n($dia, $unixtimestamp); ?>
                    <?php echo date_i18n($dianum, $unixtimestamp); ?>
                    <?php echo date_i18n($mes, $unixtimestamp); ?>
                    - <?php the_field('hora')?>
                </h5><br>
                 <?php if( get_field('data_2') ):  ?>
                 <h5 class="uk-margin-remove-top uk-margin-small-bottom uk-button uk-button-default ">
                     <span class="uk-icon-sub" uk-icon="icon: calendar"></span>
                     <?php echo date_i18n($dia, $unixtimestamp2); ?>
                    <?php echo date_i18n($dianum, $unixtimestamp2); ?>
                    <?php echo date_i18n($mes, $unixtimestamp2); ?>
                    - <?php the_field('hora_2')?>
                </h5><br>
                 <?php endif; ?> 
                 
                 <?php if( get_field('data_3') ):  ?>
                 <h5 class="uk-margin-remove-top uk-margin-small-bottom uk-button uk-button-default ">
                     <span class="uk-icon-sub" uk-icon="icon: calendar"></span>
                     <?php echo date_i18n($dia, $unixtimestamp3); ?>
                    <?php echo date_i18n($dianum, $unixtimestamp3); ?>
                    <?php echo date_i18n($mes, $unixtimestamp3); ?>
                    - <?php the_field('hora_3')?>
                </h5><br>
                 <?php endif; ?> 
                
                <span class="uk-text-small">

                    <?php if( get_field('preu_general') ):  ?>
                        <?php pll_e('General') ?>: <?php the_field('preu_general'); ?> -
                    <?php endif; ?>  
                    
                    <?php if( get_field('preu_general_indef') ):  ?>
                        <?php pll_e('General') ?>: <?php the_field('preu_general_indef'); ?> 
                    <?php endif; ?>  
                    
                    <?php if( get_field('preu_amic') ):  ?>
                        <?php pll_e('Amic') ?>: <?php the_field('preu_amic'); ?> 
                    <?php endif; ?>
                    
                    <?php if( get_field('preu_amic_indef') ):  ?><br>
                        <?php pll_e('Amic') ?>: <?php the_field('preu_amic_indef'); ?> 
                    <?php endif; ?>
                </span>
                    
            </div>

        </div><!-- //end card -->

</div><!-- //end card wrapper -->