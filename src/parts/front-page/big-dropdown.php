
<div class="uk-container uk-text-center big-dropdown" >
  
  <button class="uk-button uk-button-default uk-button big-dropdown_button event-dropdown-home" type="button">
  	<?php pll_e('És la primera vegada que ens visites?'); ?> <span class="uk-icon-sub" uk-icon="icon:  triangle-down;ratio: 1.5;"> </span> 
  </button>
  
  <div class="uk-width-xxlarge uk-padding-remove-horizontal uk-padding-remove-bottom" uk-dropdown="mode: click; animation: uk-animation-slide-top-small; duration: 200;pos: bottom-justify;">
    
    <?php big_dropdown(); ?> 
  
  </div>

</div>