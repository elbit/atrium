<?php
	$loop = new WP_Query( array(
	'post_type' => 'claim',
	'posts_per_page' => 1
	)
	);
?>

<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>

<h1 class="uk-text-center"><?php the_title('')?></h1>

<?php endwhile; wp_reset_query(); ?>