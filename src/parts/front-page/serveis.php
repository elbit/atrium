<div class="uk-container uk-text-center">
    
    <h1 class="uk-margin-remove-bottom" > <?php pll_e('T&#39;ho posem fàcil') ?></h1>
    <h3 class="uk-subtitle uk-margin-remove-top uk-margin-medium-bottom" > <?php pll_e('Serveis a la teva disposició') ?></h3>
    
    <div class="uk-child-width-1-2@s uk-child-width-1-4@m  uk-grid-match" uk-grid>
        <div>
            <a class="uk-card uk-card-default uk-card-hover uk-card-body uk-text-center bit-card-button uk-padding-remove-horizontal" href="<?php pll_e('prepara-la-visita'); ?>#acollida">
                <h3 class="uk-card-title"> <?php pll_e('Servei Acollida') ?></h3>
                <img src="<?php echo esc_url(get_template_directory_uri()) . '/img/serveis/acollida.svg'; ?>" alt="">
                
            </a>
        </div>
        
        <div>
            <a href="<?php pll_e('prepara-la-visita'); ?>#accessibilitat" class="uk-card uk-card-default uk-card-hover uk-card-body uk-text-center bit-card-button" >
                <h3 class="uk-card-title"> <?php pll_e('Accessibilitat') ?></h3>
                <img src="<?php echo esc_url(get_template_directory_uri()) . '/img/serveis/discapacitats.svg'; ?>" alt="">
                
            </a>
        </div>
        
        <div>
            <a href="<?php pll_e('prepara-la-visita'); ?>#parking" class="uk-card uk-card-default uk-card-hover uk-card-body uk-text-center bit-card-button">
                <h3 class="uk-card-title"><?php pll_e('Aparcament') ?></h3>
                <img src="<?php echo esc_url(get_template_directory_uri()) . '/img/serveis/parking.svg'; ?>" alt="">
                
            </a>
        </div>
        
        <div>
            <a href="<?php pll_e('prepara-la-visita'); ?>#guarda-roba" class="uk-card uk-card-default uk-card-hover uk-card-body uk-text-center bit-card-button">
                <h3 class="uk-card-title"><?php pll_e('Guarda-roba') ?> </h3>
                <img src="<?php echo esc_url(get_template_directory_uri()) . '/img/serveis/guarda-roba.svg'; ?>" alt="">
                
            </a>
        </div>
        
        
    </div>
</div>