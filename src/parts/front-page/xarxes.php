<div class="uk-container uk-text-center">
  
  <h1 class="uk-margin-remove-bottom"><?php pll_e('Continuem en contacte?'); ?></h1>
  <h3 class="uk-subtitle uk-margin-remove-top uk-margin-medium-bottom"><?php pll_e('Actualitzacions, notícies i més!') ?></h3>
  
  <div class="uk-flex-center" uk-grid>
    
    <div>
      <a href="https://twitter.com/AtriumTeatre" target="_blank">
        <img src="<?php echo esc_url(get_template_directory_uri()) . '/img/xarxes/twitter.png'; ?>"> 
      </a>
    </div>



    <div>
      <a href="https://www.facebook.com/AtriumViladecans" target="_blank">
        <img  src="<?php echo esc_url(get_template_directory_uri()) . '/img/xarxes/facebook.png'; ?>" alt="facebook">
      </a>
    </div>

    <div>
      <a href="https://www.youtube.com/channel/UCiA1fSus1JXRw02IT4XQjeQ" target="_blank">
        <img  src="<?php echo esc_url(get_template_directory_uri()) . '/img/xarxes/youtube.svg'; ?>" alt="Youtube ">
      </a>
    </div>

    <div>
      <a href="https://www.instagram.com/atriumviladecans" target="_blank">
        <img  src="<?php echo esc_url(get_template_directory_uri()) . '/img/xarxes/instagram.svg'; ?>" alt="Instagram">
      </a>
    </div>

    
  
  </div>
</div>