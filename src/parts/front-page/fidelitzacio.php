<?php
$loop = new WP_Query( array(
    'post_type' => 'fidelitzacio_home',
    'posts_per_page' => 1
  )
);
?>

<div class="uk-container uk-text-center" >

<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>

  <h1 class="uk-margin-remove-bottom " ><span uk-scrollspy="cls: uk-animation-scale-up;  delay: 500; repeat: true" uk-icon="icon: heart; ratio:2;" class="uk-margin-right-small uk-margin-small-right " style="color:red;"> </span><?php the_title('')?></h1>
  
  <h3 class="uk-subtitle uk-margin-remove-top" ><?php the_field('subtitol')?></h3>
  
  <div class="uk-child-width-1-4@m uk-child-width-1-2@s uk-flex-center" uk-grid>
    
    <div>
      <a class="uk-button uk-button-default uk-button-large  bit-button-dark  uk-width-1-1 uk-text-middle event-info-amic-home"  href="<?php pll_e('fes-te-amic-de-latrium') ?>" target="_blank"><?php the_field('cta_1')?></a>
    </div>
    
    <div>
      <a class="uk-button uk-button-default uk-button-large bit-button-dark uk-width-1-1 event-koobin-amic-home"  href="<?php pll_e('https://atriumviladecans.koobin.com/index.php?action=PU_zona_personal&tipo_tarifa=44&alta=true') ?>" target="_blank"><?php the_field('cta_2')?></a>
    </div>
  
  </div>


<?php endwhile; wp_reset_query(); ?>

</div>