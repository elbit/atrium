<div class="uk-container uk-margin-medium-bottom">
	
	<div class="uk-child-width-1-2@s  uk-child-width-1-4@m uk-grid-match" uk-grid>
		<div >
			<a href="espectacles/<?php pll_e('vull-passar-una-bona-estona') ?>" class="uk-card uk-card-default uk-card-hover uk-card-body uk-text-center uk-background-contain event-categoria-home-cat1 uk-background-bottom-right" data-src="<?php echo esc_url(get_template_directory_uri()) . '/img/categories/bona-estona.jpg'; ?>" uk-img>
				<h4 class="uk-card-title " > <?php pll_e('Vull passar una bona estona') ?></h4>
				
			</a>
		</div>
		<div>
			<a href="espectacles/<?php pll_e('els-imprescindibles-del-teatre') ?>" class="uk-card uk-card-default uk-card-hover uk-card-body uk-text-center uk-background-cover  event-categoria-home-cat2 uk-background-bottom-center" data-src="<?php echo esc_url(get_template_directory_uri()) . '/img/categories/teatre.jpg'; ?>" uk-img>
				<h4 class="uk-card-title">  <?php pll_e('  Els imprescindibles de teatre') ?>  </h4>
				
			</a>
		</div>
		<div>
			<a href="espectacles/<?php pll_e('per-venir-en-familia') ?>" class="uk-card  uk-card-default uk-card-hover uk-card-body uk-text-center  event-categoria-home-cat3 uk-background-bottom-right uk-background-contain" data-src="<?php echo esc_url(get_template_directory_uri()) . '/img/categories/familia.jpg'; ?>" uk-img>
				<h4 class="uk-card-title">   <?php pll_e('Per venir en família') ?></h4>
				
			</a>
		</div>
		<div >
			<a href="espectacles/<?php pll_e('descobrir-coses-noves') ?>" class="uk-card  uk-card-default uk-card-hover uk-card-body uk-text-center uk-background-cover  uk-animation-scale-up uk-transform-origin-bottom-right uk-background-bottom-right event-categoria-home-cat4 uk-background-contain" data-src="<?php echo esc_url(get_template_directory_uri()) . '/img/categories/descobrir.jpg'; ?>" uk-img>
				
				<h4 class="uk-card-title" style="color: #333">  <?php pll_e('M&#39;agrada arriscar i descobrir coses noves') ?></h4>
				
			</a>
		</div>
		
		
	</div>
	
	<div class="uk-child-width-1-2@s uk-grid-match" uk-grid>
		<div>
			
			<a href="<?php pll_e('https://atriumviladecans.koobin.com/') ?>" class="uk-card uk-card-default uk-card-hover uk-card-body uk-text-center uk-text-muted uk-background-cover bit-card-button bit-card-button-primary"  target="_blank" >
				
				<h4 class="uk-card-title event-cta-compra-categories-home" > <?php pll_e('Comprar Entrades') ?></h4>
				
			</a>
			
		</div>
		<div>
			
			<a target="_blank" href="<?php pll_e('https://atriumviladecans.com/t-atrium/') ?>"class="uuk-card  uk-card-default uk-card-hover uk-card-body uk-text-center uk-background-cover uk-background-secondary bit-card-button bit-card-button-secondary">
				<h4 class="uk-card-title" style="color: #333">   <?php pll_e('Coneixes els abonaments?') ?></h4>
				
			</a>
			
		</div>
	</div>
</div>