
<div class="uk-container uk-text-center " >
	
	<section class="uk-section-xsmall uk-margin-bottom">
	    <h1 class="uk-margin-remove-bottom uk-margin-remove-top"  ><?php pll_e('Què tenim?') ?></h1>
	    <h3 class="uk-subtitle uk-margin-remove-top uk-text-default" ><?php pll_e('Selecciona quan vols venir o què vols veure') ?></h3>
    </section>

    <div class="uk-child-width-1-3@s uk-grid-match" uk-grid>
			<div>
				
				 <a href="espectacles/aquesta-setmana" class="uk-card uk-card-default uk-card-hover uk-padding uk-text-center uk-text-middle uk-h3 bit-card-button">
				 	<img src="<?php echo esc_url(get_template_directory_uri()) . '/img/calendar/setmana-n.svg'; ?>" alt="">
					<?php pll_e('Aquesta setmana') ?>
				 
	        	</a>
	        	<!--  <a href="espectacles/#filter=<?php//pll_e('.octubre') ?>" class="uk-card uk-card-default uk-card-hover uk-padding uk-text-center uk-text-middle uk-h3 bit-card-button event-calendar-home-1">
				 	<img src="<?php //echo esc_url(get_template_directory_uri()) . '/img/calendar/octubre.png'; ?>" alt="">
					<?php //pll_e('octubre') ?>
				 
	        	</a> -->
			</div>
			<div>
		        <a href="espectacles/aquest-mes" class="uk-card uk-card-default uk-card-hover uk-padding uk-text-large uk-text-center uk-text-middle bit-card-button">  
		        	<img src="<?php echo esc_url(get_template_directory_uri()) . '/img/calendar/mes-n.svg'; ?>" alt="">
		        		<?php pll_e('Aquest mes') ?>
		        </a>

		        <!-- <a href="espectacles/#filter=<?php //pll_e('.novembre') ?>" class="uk-card uk-card-default uk-card-hover uk-padding uk-text-large uk-text-center uk-text-middle bit-card-button event-calendar-home-2">  
		        	<img src="<?php //echo esc_url(get_template_directory_uri()) . '/img/calendar/novembre.png'; ?>" alt="">
		        		<?php //pll_e('novembre') ?>
		        </a> -->
		        
	        </div>
	        <div>
		        <a href="espectacles/proper-mes" class="uk-card uk-card-default uk-card-hover uk-padding uk-text-large  uk-text-center uk-text-middle bit-card-button"> 
		        <img src="<?php echo esc_url(get_template_directory_uri()) . '/img/calendar/proper-mes-n.svg'; ?>" alt=""> 
		        	<?php pll_e('El proper mes') ?></a>

		       <!--  <a href="espectacles/#filter=<?php //pll_e('.desembre') ?>" class="uk-card uk-card-default uk-card-hover uk-padding uk-text-large  uk-text-center uk-text-middle bit-card-button event-calendar-home-3"> 
		        <img src="<?php //echo esc_url(get_template_directory_uri()) . '/img/calendar/desembre.png'; ?>" alt=""> 
		        	<?php //pll_e('desembre') ?></a> -->
		        
	        </div>
			
    </div>
    
    <section class="uk-section-small uk-padding-remove-bottom">	
	    <div class="uk-width-1-1 uk-card uk-card-default uk-card-hover uk-text-center uk-text-middle uk-padding text-big">  
			<a class="uk-text-large bit-card-button event-calendar-home-general" href="espectacles" ><?php pll_e('Consulta tota la programació') ?></a>
		</div>
	</section>	

 </div>