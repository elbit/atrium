<?php
$loop = new WP_Query( array(
    'post_type' => 'espectacles',
    'posts_per_page' => 4,
    'orderby' => 'meta_value',
    'meta_key' => 'data',
    'order' => 'ASC',
    'tax_query' => array(
        array(
            'taxonomy' => 'category',
            'field' => 'slug',
            'terms' => 'destacat'
        )
     )
    )
);
?>


<div class="uk-container" >

    <div class="uk-child-width-expand@m uk-child-width-1-2@s uk-grid-large uk-grid-match" uk-grid>

    <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
        
        <div class="">
            
            <div <?php post_class(' uk-card uk-card-default espectacle-destacat'); ?>  >
                
                <div class="uk-card-body uk-padding-remove ">
                        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                            <?php the_post_thumbnail( 'espectacles-destacats' ); ?>
                        </a>
                </div>
            
                <div class="uk-card-footer uk-flex uk-flex-center  uk-padding-small">
                    
                    <a  class="uk-button uk-button-secondary uk-button-small uk-width-1-2 uk-button-vw espectacle-destacat_button" href="<?php the_permalink(); ?>">+ info</a>
                    
                    <a class="uk-button uk-button-primary uk-button-small uk-width-1-2 uk-button-vw espectacle-destacat_button " href="<?php the_field('enllas_koobin'); ?>" target="_blank">Comprar</a>
                </div>
                
                <div class="uk-card-footer uk-text-center  uk-padding-small">
                    <h4 class=""><?php the_title('')?></h4>
                </div>
            
            </div>
        </div>


    <?php endwhile; wp_reset_query(); ?>
    </div>
</div>

