
<?php			// WP_Query arguments
	$args = array (
	'posts_per_page'         => '8',

	);

	// The Query
	$query = new WP_Query( $args );

?>


<?php if ( $query->have_posts() ) { 
	while ( $query->have_posts() ) {
	$query->the_post();
?>
	
<div class="excerpt uk-width-1-2@s ">

	<h4 class="excerpt-title "><a href="<?php the_permalink(); ?>"> <?php the_title();?></a> <br><span class="excerpt-date uk-text-small"> <?php the_date();?></span> </h4>

	<div class="uk-margin-small">
	<?php the_post_thumbnail('');  ?>
	</div>

	<div><?php the_excerpt();?></div>
<hr>
</div>



<?php
	}
} else {
	// no posts found
}

// Restore original Post Data
wp_reset_postdata();


?>