
 <?php if( !get_field('data_2') ): //Soy la fecha 1 alone ?>

<div class="espectacle-dates uk-flex-middle uk-flex-center " uk-grid>
        
   <!--  <div class="uk-width-1-5 ">
        <img src="<?php echo esc_url(get_template_directory_uri()) . '/img/calendar/setmana-small.png'; ?>" alt="">
    </div> -->
    <div class="uk-width-1-3 ">
        <ul class="uk-grid-divider  uk-list uk-list-small uk-text-center">
        <?php 
            $mes = "F";
            $dia = "l";
            $dianum = "j";
            $any = "Y";
            $unixtimestamp = strtotime(get_field('data'));
            ?>	
           
            <li class="dia  uk-text-capitalize uk-h3"><?php echo date_i18n($dia, $unixtimestamp); ?></li>
            <li class="dia-num uk-h1 uk-text-bold uk-text-secondary"><?php echo date_i18n($dianum, $unixtimestamp); ?></li>
            <li class="mes uk-text-uppercase uk-h4"><?php echo date_i18n($mes, $unixtimestamp); ?></li>
            <li class="uk-text-muted uk-h4"><?php the_field('hora'); ?></li>
        </ul>

    </div>
</div>

<hr>

<a href="<?php echo get_home_url(); ?><?php pll_e('/prepara-la-visita'); ?>" class="uk-child-width-1-5 uk-flex-middle uk-flex-center" uk-grid>
    <div>
        <img class="uk-border-circle" src="<?php the_field('imatge_serveis_1'); ?>" alt="">
    </div>
                           
    <div>
        <img class="" src="<?php the_field('imatge_serveis_2'); ?>" alt="">
    </div>
</a>

<?php endif; ?>

   
<?php if( get_field('data_2') ):  //fecha 1 + 2?>

<div class="uk-grid-divider uk-child-width-1-2 espectacle-dates flex-center " uk-grid>
     
     <ul class="uk-list uk-list-small uk-text-center uk-margin-top">
        <?php  //Soy la fecha 1 acompañada
            $mes = "F";
            $dia = "l";
            $dianum = "j";
            $any = "Y";
            $unixtimestamp = strtotime(get_field('data'));
            ?>  
       
        <li class="dia  uk-text-capitalize uk-h3"><?php echo date_i18n($dia, $unixtimestamp); ?></li>
        <li class="dia-num uk-h1 uk-text-bold uk-text-secondary"><?php echo date_i18n($dianum, $unixtimestamp); ?></li>
        <li class="mes uk-text-uppercase uk-h4"><?php echo date_i18n($mes, $unixtimestamp); ?></li>
        <li class="uk-text-muted uk-h4"><?php the_field('hora'); ?></li>
    </ul>

    <ul class="uk-list uk-text-center uk-margin-top">
    
        <?php 
        $mes = "F";
        $dia = "l";
        $dianum = "j";
        $any = "Y";
        $unixtimestamp = strtotime(get_field('data_2'));
        ?>	
        
        <li class="dia  uk-text-capitalize uk-h3"><?php echo date_i18n($dia, $unixtimestamp); ?></li>
        <li class="dia-num uk-h1 uk-text-bold uk-text-secondary"><?php echo date_i18n($dianum, $unixtimestamp); ?></li>
        <li class="mes  uk-text-uppercase uk-h4"><?php echo date_i18n($mes, $unixtimestamp); ?></li>
        <li class="uk-text-muted uk-h4"><?php the_field('hora_2'); ?></li>
    
    </ul>
</div>
<hr class="uk-divider-icon">


    <?php if( get_field('data_3') ): //Soy la fecha 3 ?>

    <div class="uk-grid-divider uk-child-width-1-2 espectacle-dates flex-center " uk-grid>
        
        <ul class="uk-list uk-list-small uk-text-center uk-margin-top">
            <?php 
            $mes = "F";
            $dia = "l";
            $dianum = "j";
            $any = "Y";
            $unixtimestamp = strtotime(get_field('data_3'));
            ?>  

            <li class="dia  uk-text-capitalize uk-h3"><?php echo date_i18n($dia, $unixtimestamp); ?></li>
            <li class="dia-num uk-h1 uk-text-bold uk-text-secondary"><?php echo date_i18n($dianum, $unixtimestamp); ?></li>
            <li class="mes  uk-text-uppercase uk-h4"><?php echo date_i18n($mes, $unixtimestamp); ?></li>
            <li class="uk-text-muted uk-h4"><?php the_field('hora_3'); ?></li>
        </ul>

        <div class="uk-child-width-1-2 uk-flex-middle uk-flex-center" uk-grid>
            <div>
                <img class="uk-border-circle" src="<?php the_field('imatge_serveis_1'); ?>" alt="">
            </div>
                               
            <div>
                <img class="" src="<?php the_field('imatge_serveis_2'); ?>" alt="">
            </div>
         </div>

            
    </div>

    
        
    <?php endif; ?>


     <?php if( !get_field('data_3') ): //Soy la fecha 3 ?>
    <div class="uk-child-width-1-5 uk-flex-middle uk-flex-center" uk-grid>
        <div>
            <img class="uk-border-circle" src="<?php the_field('imatge_serveis_1'); ?>" alt="">
        </div>
                           
        <div>
            <img class="" src="<?php the_field('imatge_serveis_2'); ?>" alt="">
        </div>
    </div>
    <?php endif; ?>

<?php endif; ?>

 

