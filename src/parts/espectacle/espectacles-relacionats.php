<?php

$post_categories =  wp_get_post_categories( $post->ID);
$today = date('Ymd');//comentar x inabilitar
$loop = new WP_Query( array(
    
    'post__not_in' => array( get_the_ID() ),
    'post_type' => 'espectacles',
    'posts_per_page' => 4,
    'orderby' => 'rand',
    'cat' => $post_categories,
    'meta_key' => 'data',
    'meta_query'     => array(
            array(
                'key'     => 'data',
                'compare' => '>=',
                'value'   => $today,


            ),
            
    ),

  )
);
?>


<div class="uk-container" >
    <div class="uk-section">
    
    <h2><?php pll_e('També et pot interessar:') ?></h2>
 

    <div class="uk-child-width-1-4@m uk-child-width-1-2@s uk-grid-medium uk-grid-match" uk-grid>

    <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
       

        <div class="flt" 
            category="
            <?php 
                foreach((get_the_category(array('cat' => '-1'))) as $category) { 
                    //this would print cat names.. You can arrange in list or whatever you want..
                    echo 'category-'.$category->slug .',';
                } 
                ?>"
        >
            <div <?php post_class(' uk-card uk-card-default espectacle-destacat filtr-item '); ?>  >
                
                
                <div class="uk-card-body uk-padding-remove">
                        <?php //foreach((get_the_category( )) as $category) { 
                    //this would print cat names.. You can arrange in list or whatever you want..
                   // echo $category->name ;} 
                ?>
                    <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                            <?php the_post_thumbnail( 'espectacles-destacats' ); // Declare pixel size you need inside the array. ?>
                    </a>
                </div>
            
                <div class="uk-card-footer uk-flex uk-flex-center uk-padding-small" >
                    
                   <a class="uk-button uk-button-secondary uk-button-small uk-width-1-2" href="<?php the_permalink(); ?>">+ info</a>


                  
                    <a class="uk-button uk-button-primary uk-button-small uk-width-1-2 event-cta-compra-fitxa-relacionats" href="<?php the_field('enllas_koobin'); ?>" target="_blank">Comprar</a>
                   
                </div>
                
                <div class="uk-card-footer uk-text-center">
                    <h4 class=""><?php the_title('')?></h4>
                </div>
            
            </div>
        </div>


    <?php endwhile; wp_reset_query(); ?>
    </div></div>
</div>

