<?php if( get_field('video') ): ?>
    <div class="uk-responsive video" >
     <?php echo wp_oembed_get( get_field( 'video' ) ); ?>
    </div>
<?php endif; ?>