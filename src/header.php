<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		
		<title><?php wp_title( '' ); ?><?php if ( wp_title( '', false ) ) { echo ' : '; } ?><?php bloginfo( 'name' ); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
		<link href="<?php echo esc_url( get_template_directory_uri() ); ?>/img/icons/favicon.ico" rel="shortcut icon">
		<link href="<?php echo esc_url( get_template_directory_uri() ); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">
		<link rel="alternate" type="application/rss+xml" title="<?php bloginfo( 'name' ); ?>" href="<?php bloginfo( 'rss2_url' ); ?>" />

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo( 'description' ); ?>">
 

		<?php wp_head(); ?>
		
		<script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-rc.4/js/uikit.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-rc.4/js/uikit-icons.min.js"></script>

		<script>
			(function(w,d,t,u,n,a,m){w['MauticTrackingObject']=n;
				w[n]=w[n]||function(){(w[n].q=w[n].q||[]).push(arguments)},a=d.createElement(t),
				m=d.getElementsByTagName(t)[0];a.async=1;a.src=u;m.parentNode.insertBefore(a,m)
			})(window,document,'script','https://atriumviladecans.tekneaudience.com/mtc.js','mt');	
			mt('send', 'pageview');
		</script>

	</head>
	<body <?php body_class(); ?>>

		<div class="uk-section-xsmall uk-padding-small site-header">
			
			<header class="" role="banner" uk-grid>
				
				<div class=" uk-padding-top logo">
					<a href="<?php echo esc_url( home_url() ); ?>">
						<img src="<?php echo esc_url(get_template_directory_uri()) . '/img/logos/logo.svg'?>" alt="" style="max-width: 250px;">
					</a>
				</div>
				
				<div class="uk-position-top-right uk-margin-right lang-switcher uk-text-nowrap" style="z-index:1000">
					<?php pll_the_languages();	 ?>
				</div>
					
				<nav class="uk-width-expand@s uk-navbar-container uk-navbar-transparent nav-bar" role="navigation"  uk-navbar >

					<div class="uk-navbar-right" style="font-size:9px">
						
						<?php html5blank_nav(); ?>
						
					</div>
				</nav>
			
			</header>
			
		</div>