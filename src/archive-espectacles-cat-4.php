<?php get_header(); /*
 * Template Name: archive categories_home_cat4
 * Template Post Type: espectacles, page
 */?>

	<main role="main" aria-label="Content" class="uk-container main">
			
			<h1><?php pll_e('Descobrir coses noves'); ?> </h1>
       
       <section class="flt-groups uk-container uk-margin-top">
	   		
            
        	<?php get_template_part( '/parts/programacio/filtres/filtre-calendari_menu' ); ?>
			
					
        </section>

        <section class="uk-section-xsmall uk-margin-bottom-remove flt-m" >
            
            <?php get_template_part( '/parts/programacio/programacio-cat-4' ); ?>
        
        </section>

    </main>

<?php get_template_part( '/parts/programacio/filtres/isotope-js' ); ?>

<?php get_footer(); ?>



	
	
		

