<?php get_header(); 
 /*
 * Template Name: Bloc
 * Template Post Type: page
 */
?>

	<main class="uk-container main"role="main" aria-label="Content" ">
		
		<section class="uk-margin-large-bottom">

			<h1><?php the_title(); ?></h1>

			<div class=" uk-grid-divider" uk-grid>

				<?php get_template_part( '/parts/page-templates/blog-excerpt' ); ?>

			</div>

			<div class="navigation"><p><?php posts_nav_link(); ?></p></div>

		</section>
		
	</main>



<?php get_footer(); ?>
