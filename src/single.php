<?php get_header(); ?>

	<main role="main" aria-label="Content" class="uk-container uk-container-small main main-blog">
	
	<section>


	<?php if ( have_posts() ) : while (have_posts() ) : the_post(); ?>

	
		<article id="post-<?php the_ID(); ?>" <?php post_class('uk-article article'); ?>>

			
			<!-- <?php //if ( has_post_thumbnail() ) : // Check if Thumbnail exists. ?>
				<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
					<?php the_post_thumbnail(); // Fullsize image for the single post. ?>
				</a>
			<?php //endif; ?> -->
			
			
			<h1>
				<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
			</h1>	

			
			<span class="date">
				<time datetime="<?php the_time( 'Y-m-d' ); ?> <?php //the_time( 'H:i' ); ?>">
					<?php the_date(); ?>
				</time>
			</span>
			
			<!-- <span class="author"> <?php the_author_posts_link(); ?></span> -->
			
			<?php the_content(); ?>

			




			<?php //the_tags( __( 'Tags: ', 'html5blank' ), ', ', '<br>' ); // Separated by commas with a line break at the end. ?>

			<p><?php //esc_html_e( 'Categorised in: ', 'html5blank' ); the_category( ', ' ); // Separated by commas. ?></p>

			

			<?php edit_post_link();  ?>

			<?php //comments_template(); ?>

		</article>
		

	<?php endwhile; ?>

	<?php else : ?>
			<article>

				<h1><?php esc_html_e( 'Sorry, nothing to display.', 'html5blank' ); ?></h1>

			</article>
	<?php endif; ?>

		</section>
	
	</main>


<script src="https://cdnjs.cloudflare.com/ajax/libs/fitvids/1.2.0/jquery.fitvids.js"></script>
<script>
  jQuery(document).ready(function(){
    
    //jQuery("iframe").fitVids();
    jQuery('iframe').parent().fitVids();
  });
</script>

<?php get_footer(); ?>

