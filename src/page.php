<?php get_header(); ?>

	<main role="main" aria-label="Content" class="uk-container uk-container-small main" >
		<!-- section -->
		<section>
			<?php if ( have_posts()) : while ( have_posts() ) : the_post(); ?>
			<!-- article -->
			<article id="post-<?php the_ID(); ?>" <?php post_class('uk-article article'); ?>>
				
				<h1 class="uk-article-title"><?php the_title(); ?></h1>
				
				<?php the_content(); ?>

				<?php //comments_template( '', true ); // Remove if you don't want comments. ?>

				<br class="clear">

				<?php edit_post_link(); ?>

			</article>
			<!-- /article -->

		<?php endwhile; ?>

		<?php else : ?>

			<!-- article -->
			<article>

				<h2><?php esc_html_e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

			</article>
			<!-- /article -->

		<?php endif; ?>

		</section>
		<!-- /section -->
	</main>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/fitvids/1.2.0/jquery.fitvids.js"></script>
<script>
  jQuery(document).ready(function(){
    
    
	jQuery('iframe[src*="youtube"]').parent().fitVids();
  });
</script>

<?php get_footer(); ?>
