<?php get_header(); ?>

	<main role="main" aria-label="Content" class="uk-container espectacle main">

		<ul class="uk-pagination uk-margin-remove-bottom">
			<li><a  onclick="history.go(-1);" target="_blank" rel="noopener noreferrer"><span class="uk-margin-small-right" uk-pagination-previous></span> <?php pll_e('Tornar enrere') ?></a></li>
		</ul>
    		
	<?php if ( have_posts() ) : while (have_posts() ) : the_post(); ?>
		
		<section class="uk-section-xsmall  uk-padding-remove-bottom espectacle-header" >
			<div class="uk-child-width-1-2@s"  uk-grid>
				<div>
					<h1 class="uk-margin-remove-bottom espectacle-title"><?php the_title(); ?></h1>
			
					<?php if( get_field('artista/companyia') ): ?>
						<h2 class="uk-text-muted uk-margin-remove-top espectacle-subtitle"> <?php the_field('artista/companyia'); ?></h2>
					<?php endif; ?>
				</div>	
			
				<?php //if( get_field('enllas_koobin') ): ?>
					<div>
						<a class="uk-button uk-button-primary uk-button-large uk-width-1-1 uk-text-large uk-padding-small espectacle-big-cta event-cta-compra-fitxa-big" href="<?php the_field('enllas_koobin'); ?>" target="_blank"><?php pll_e('Comprar entrades') ?></a>
					</div>

				<?php //endif; ?>
			</div>
		</section>

		<hr>
		
		<section class="uk-section-xsmall" >
			
			<div class=" uk-child-width-1-2@s uk-flex-middle uk-grid-divider" uk-grid>

				<div class="espectacle-video uk-responsive-width">	
					
					<?php if( get_field('video') ): ?>
						
						<?php get_template_part( '/parts/espectacle/video' ); ?>
					
					<?php else : ?>

						 <?php the_post_thumbnail( 'espectacle-single' );?>

					<?php endif; ?>
				</div>
				
				<div>	
					
					<?php get_template_part( '/parts/espectacle/dates' ); ?>
				</div>
				
			</div>
		
		</section>
		
		<hr class="uk-divider-icon uk-margin-small">
		
		<section class="uk-section uk-grid-divider uk-child-width-1-2@m"  uk-grid>

			<div class="uk-article">

				 <h1 class="uk-article-title"><?php pll_e('Sinopsi') ?></h1>

				<?php the_field('sinopsi'); ?>
				
				<?php if( get_field('sinopsi_extra') ):  ?>

				<ul uk-accordion>
					<li style="border: 1px solid #999;" class="uk-padding-small">
						<a class="uk-accordion-title" href="#"><?php pll_e('Informació adicional') ?></a>
						<div class="uk-accordion-content">
						<?php the_field('sinopsi_extra'); ?>
						</div>
					</li>
				</ul>
				<?php endif; ?>
			</div>
			<div>
				<ul class="uk-margin-small-bottom ">
					<?php if( get_field('durada') ):  ?>
					<li>  </span> <span class="uk-text-bold"><?php pll_e('Durada') ?>: </span>	 <?php the_field('durada'); ?></li>
					<?php endif; ?>
					
					<?php if( get_field('idioma') ):  ?>
					<li> <span class="uk-text-bold"><?php pll_e('Idioma') ?>: </span>  <?php the_field('idioma'); ?></li>
					<?php endif; ?>
					
					<?php if( get_field('edat_recomanada') ):  ?>
					<li> <span class="uk-text-bold"><?php pll_e('Edat recomanada') ?>: </span>  <?php the_field('edat_recomanada'); ?> </li>
					<?php endif; ?>
					
					<?php if( get_field('mes_informacio') ):  ?>
					<li><a class="uk-text" href="<?php the_field('mes_informacio'); ?>" target="_blank"><?php pll_e('Més informació') ?></a><span class="uk-icon uk-margin-small-left" uk-icon="icon:forward; ratio:0.8;"></span></li>
					<?php endif; ?>
				
				</ul>
				<hr >
				
				<div class="uk-margin-small-bottom">
					
					<div class="uk-child-width-1-2 uk-text-center espectacle-preu" uk-grid> 
						
						<?php if( get_field('preu_general') ):  ?>
						<div>
							<span class="uk-text-bold uk-text-large "><?php pll_e('Preu General') ?>: <?php the_field('preu_general'); ?></span>
						</div>

						<?php endif; ?>  
						
						<?php if( get_field('preu_general_indef') ):  ?>
						<div>
							<span class="uk-text-bold uk-text-large "><?php pll_e('Preu General') ?>: <br><?php the_field('preu_general_indef'); ?></span>
						</div>
						<?php endif; ?>  
						
						<?php if( get_field('preu_amic') ):  ?>
						<div>
							<span class="uk-text-bold uk-text-large "><?php pll_e('Preu Amic') ?>: <?php the_field('preu_amic'); ?> </span>  
						</div>
						<?php endif; ?>
						
						<?php if( get_field('preu_amic_indef') ):  ?>
						<div>
							<span class="uk-text-bold uk-text-large "><?php pll_e('Preu Amic') ?>: <br> <?php the_field('preu_amic_indef'); ?></span>  
						</div>
						<?php endif; ?>
					</div>
					
					<div class="uk-child-width-1-2 uk-text-center uk-grid-small uk-grid-match" uk-grid>
						<div class="">
							<a href="<?php the_field('enllas_koobin');  ?>" class="uk-button uk-button-primary uk-width-1-1 uk-padding-small uk-text-small uk-flex-middle uk-text-middle event-cta-compra-fitxa-small" target="_blank"><?php pll_e('Comprar entrades') ?></a>
						</div>
						<div>
							<a href="<?php pll_e('https://atriumviladecans.com/t-atrium/') ?>" class="uk-button uk-button-secondary uk-width-1-1 uk-text-small uk-padding-small" target="_blank"><?php pll_e('Coneixes els abonaments?') ?></a>
						</div>
					</div>
					
				</div>
				
				<hr>
				
					<ul class="uk-list">

						<?php if( get_field('servei_extra') ): ?>
						
						<li style="border: solid 1px #999 ; font-weight:200; font-size:0.8rem;" class="uk-card uk-padding-small uk-border uk-text-small uk-clearfix uk-flex-middle">
							<img src="<?php the_field('imatge_servei_extra'); ?>" alt="" class="uk-float-left uk-width-1-6" >
							<span class="uk-float-right uk-width-4-5 "><?php the_field('servei_extra'); ?></span>
						</li>

						<?php endif; ?>
						
					</ul> 
			</div>	
		</section>
		
		
	<?php endwhile; endif; ?>

</main>

<section>
	<?php get_template_part( '/parts/espectacle/espectacles-relacionats' ); ?>
</section>


<script src="https://cdnjs.cloudflare.com/ajax/libs/fitvids/1.2.0/jquery.fitvids.js"></script>
<script>
  jQuery(document).ready(function(){
    
    jQuery(".video").fitVids();
  });
</script>

<?php get_footer(); ?>

