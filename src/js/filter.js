
///make reset clean tempate

Isotope.Item.prototype._create = function() {
				// assign id, used for original-order sorting
				this.id = this.layout.itemGUID++;
				// transition objects
				this._transn = {
								ingProperties: {},
								clean: {},
								onEnd: {}
				};
				this.sortData = {};
};

Isotope.Item.prototype.layoutPosition = function() {
				this.emitEvent('layout', [this]);
};

Isotope.prototype.arrange = function(opts) {
				// set any options pass
				this.option(opts);
				this._getIsInstant();
				// just filter
				this.filteredItems = this._filter(this.items);
				// flag for initalized
				this._isLayoutInited = true;
};

// layout mode that does not position items
Isotope.LayoutMode.create('none');





// init Isotope /// FILTER FOR MONTHS calendar
//////////////////////////////////////

var $gridM = $('.flt-m').isotope({
				itemSelector: '.flt-item-m',
				layoutMode: 'none'
});



$('.flt-button-group_calendar').on('click', '.selector', function() {
				var $this = $(this);
				// get group key
				var $buttonGroup = $this.parents('.flt-button-group');
				var filterGroup = $buttonGroup.attr('data-filter-group');
				// set filter for group
				filters[filterGroup] = $this.attr('data-filter');
				// combine filters
				var filterValue = concatValues(filters);
				// set filter for Isotope
				$gridM.isotope({ filter: filterValue });
});


// filter Categories
///////////////////////

var $grid = $('.flt').isotope({
				itemSelector: '.flt-item',
				layoutMode: 'none'
});

// store filter for each group
var filters = {};

$('.flt-groups').on('click', '.selector', function() {
				var $this = $(this);
				// get group key
				var $buttonGroup = $this.parents('.flt-button-group');
				var filterGroup = $buttonGroup.attr('data-filter-group');
				// set filter for group
				filters[filterGroup] = $this.attr('data-filter');
				// combine filters
				var filterValue = concatValues(filters);
				// set filter for Isotope
				$grid.isotope({ filter: filterValue });
});

//	change is-checked class on buttons
$('.flt-button-group,.flt-button-group_calendar').each(function(i, buttonGroup) {
				var $buttonGroup = $(buttonGroup);
				$buttonGroup.on('click', '.selector', function() {
								$buttonGroup.find('.is-checked').removeClass('is-checked');
								$(this).addClass('is-checked');
				});
});

// flatten object by concatting values
function concatValues(obj) {
				var value = '';
				for (var prop in obj) {
								value += obj[prop];
				}
				return value;
};


///HASH/////
////////////

function getHashFilter() {
				//var hash = location.hash;
				
				//get filter=filterName
				var matches = location.hash.match(/filter=([^&]+)/i);
				var hashFilter = matches && matches[1];
				return hashFilter && decodeURIComponent(hashFilter);
				return


}

$(function() {
				var $grid = $('.flt');

				// bind filter button click
				var $filters = $('.flt-button-group').on('click', '.selector', function() {

								var $buttonGroup = $this.parents('.flt-button-group');
								var filterGroup = $buttonGroup.attr('data-filter-group');
								// set filter for group
								filters[filterGroup] = $this.attr('data-filter-group');
								// combine filters
								var filterValue = concatValues(filters);
								// set filter for Isotope
								$grid.isotope({ filter: filterValue });
								// set filter in hash
								location.hash = 'filter=' + encodeURIComponent(filterAttr);

			});
				
				var isIsotopeInit = false;

				Isotope.LayoutMode.create('none');

				function onHashchange() {
								var hashFilter = getHashFilter();
								if (!hashFilter && isIsotopeInit) {
												return;
								}
								isIsotopeInit = true;
								// filter isotope
								$grid.isotope({
												itemSelector: '.flt-item',
												filter: hashFilter,
												layoutMode: 'none'
								});
								// set selected class on button
								if (hashFilter) {
												$filters.find('.is-checked').removeClass('is-checked');
												$filters.find('[data-filter="' + hashFilter + '"]').addClass('is-checked');

								}
				}

				$(window).on('hashchange', onHashchange);
				// trigger event handler to init Isotope
				onHashchange();

				var visibleItemsCount = $grid.data('isotope').filteredItems.length;

					  if( visibleItemsCount == 0 ){
					    $('.no-results').show();
					  }
					  
				
				console.log(visibleItemsCount)
});

// ///alert
// $grid.on('arrangeComplete', function(event, filteredItems) {
// 				var resultCount = filteredItems.length;
// 				if (resultCount == 0) {
// 								$grid.html("<h4>Ho hi han resultats amb aquesta selecció</h4>");
// 				}
// });


//$grid.isotope({ filter: '.flt' });

  // if ( !$grid.data('isotope').filteredItems.length ) {
  //   $('.no-results').fadeIn('slow');
  // } else {
  //   $('.no-results').fadeOut('fast');
  // }


//   $grid.isotope({
//     onLayout: function() {
//         if ( $('.flt-item').length() == 0 ) { 
//             $('.no-results').fadeIn() 
//         } else {
//             $('.no-results').fadeOut()
//         }
//         return console.log(length)
//     }
// });


  	
