<?php get_header(); ?>

	<main role="main" aria-label="Content" class="uk-container uk-container-small">
		<!-- section -->
		<section <?php post_class('uk-article article'); ?>>

			<h1><?php echo sprintf( __( '%s Search Results for ', 'html5blank' ), $wp_query->found_posts ); echo get_search_query(); ?></h1>

			<?php get_template_part( 'loop' ); ?>

			<?php get_template_part( 'pagination' ); ?>

		</section>
		<!-- /section -->
	</main>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
