	
<?php
$current_language = pll_current_language(); 
$logo_bo_image_path = get_stylesheet_directory_uri() . '/img/logos/bo-' . $current_language . '.png';
?>
<footer class="uk-section uk-padding site-footer" role="contentinfo">
	<div class="uk-flex-between uk-flex-middle" uk-grid>
		
		<div class="uk-text-small uk-width-1-1 uk-width-1-3@m bit-dark-link " style="padding-left:2;">
		<img src="<?php echo esc_url(get_template_directory_uri()) . '/img/logos/espai_cultura.svg' ?>" alt="espai cultura segura"  style="width:150px;vertical-align:baseline; mxargin-left:10px"> 
		<img src="<?php echo esc_url( $logo_bo_image_path ); ?>" alt="bo cultural"  style="height:65px;vertical-align:baseline; margin-left:10px"> 
		<br>
			&copy; <?php echo esc_html( date( 'Y' ) ); ?> <?php bloginfo( 'name' );  ?>  
				<br> 
				<span class="uk-icon-sub "uk-icon="icon: location;ratio: 0.8;"> </span> <a class="bit-dark-link" href="">Av. Josep Tarradellas, 17</a> | <span class="uk-icon-sub uk-text-nowrap"uk-icon="icon:  receiver; ratio: 0.8;"> </span><a class="bit-dark-link uk-text-nowrap" href="telf:+34936594160"> +34 93 659 41 60</a><br>
			 <span class="uk-icon-sub "uk-icon="icon: mail"> </span> <a class="bit-dark-link" href="mailto:atriumviladecans@atriumviladecans.com">atriumviladecans@atriumviladecans.com</a> <br>
			<a  href="<?php pll_e('/politica-de-privacitat') ?>"><?php pll_e('Política de Privacitat') ?></a> | <a href="<?php pll_e('/politica-de-cookies') ?>"><?php pll_e('Política de Cookies') ?></a><br>
			
		</div>
		
		<div class="uk-width-2-3@m" style="padding-left:1rem; borxder:1px red solid"> 
			
			<h5 class="uk-margin-small-bottom"><?php pll_e('Empreses col·laboradores:') ?></h5>
			<div class="uk-chixld-width-1-2@s " uk-grid>

				<div class="uk-flex uk-flex-middle uk-width-2-3@s uk-chixld-width-1-2@s uk-flex-wrap " style="border-right:1px solid grey; padding-right:0.5rem">
					
						<a href="https://www.unilever.es/" class="uk-width-1-4@s uk-width-1-2 uk-margin-xmedium-right " target="_blank" style="padding-right:25px">
							<img src="<?php echo esc_url(get_template_directory_uri()) . '/img/logos/unilever.svg' ?>" alt="" class=" ">
						</a>
					
						<a href="http://www.chocoweb.com/" class="uk-width-1-4@s uk-width-1-2 uk-marginx-medium-right " target="_blank" style="padding-right:25px">
							<img src="<?php echo esc_url(get_template_directory_uri()) . '/img/logos/nederland.svg' ?>" alt="" class=" ">
						</a>
					
						<a href="https://www.altima-sfi.com/" class="uk-width-1-4@s uk-width-1-2 uk-marginx-medium-right " target="_blank" style="padding-right:25px">
							<img src="<?php echo esc_url(get_template_directory_uri()) . '/img/logos/logo_altima.svg' ?>" alt="" class=" ">
						</a>
					
						<a href=" " class="uk-width-1-4@s uk-width-1-2 uk-marginx-medium-right " target="_blank" style="padding-right:25px">
							<img src="<?php echo esc_url(get_template_directory_uri()) . '/img/logos/geyser.svg' ?>" alt="" class=" ">
						</a>
		
				</div>
				
				<div class="uk-padding uk-width-1-3@s uk-flex uk-flex-middle" style="padding-right:0;">
					<a href="<?php pll_e('https://www.viladecans.cat') ?>" target="_blank">
						<img src="<?php echo esc_url(get_template_directory_uri()) . '/img/logos/logo-ajuntament-viladecans-2021.svg' ?>" alt="">
					</a>
				</div>

			</div>
			
			
		</div>
	
	</div>
</footer>		

<?php wp_footer(); ?>
		
		<!-- analytics -->
		<script>
		(function(f,i,r,e,s,h,l){i['GoogleAnalyticsObject']=s;f[s]=f[s]||function(){
		(f[s].q=f[s].q||[]).push(arguments)},f[s].l=1*new Date();h=i.createElement(r),
		l=i.getElementsByTagName(r)[0];h.async=1;h.src=e;l.parentNode.insertBefore(h,l)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-17072338-1', 'atriumviladecans.com');
		ga('send', 'pageview');
		</script>	

	</body>
</html>
