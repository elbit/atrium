<?php get_header(); /*
 * Template Name: servicios
 * Template Post Type: espectacles, page
 */?>

	<main role="main" aria-label="Content" class="uk-container uk-container-small uk-article" style="min-height: 75vh;">
		
		<h1><?php the_title(); ?></h1>

		<section class="">

			<h2>Servicios</h2>
			
			<p>Atrium Viladecans quiere hacer única la experiencia de quienes visitan su teatro, es por eso que ofrece los siguientes servicios a sus usuarios:</p>
			
			<ul class="uk-width-1-1 bit-serveis-list" uk-grid>
			 		
			 		<li class="" id="acollida">
			 			<img class="" src="<?php echo esc_url(get_template_directory_uri()) . '/img/serveis/acollida.png'?>" alt="" />
			 		
			 		
			 		<p><b>Servicio de acogida</b> para niñas y niños de 3 a 11 años.
Disponible en los espectáculos que muestren el icono correspondiente. Hay que solicitarlo con un mínimo de 5 días de antelación al mail acollida@atriumviladecans.com. <br><strong class="uk-text-small uk-font-underline" style="text-decoration:underline"> Servicio temporalmente fuera de servicio siguiendo las indicaciones sanitarias por la Covid-19</strong></p>

			 		</li>
				
				
			 	
			 	<li id="accessibilitat"><img class=" wp-image-187" src="<?php echo esc_url(get_template_directory_uri()) . '/img/serveis/discapacitats.png'?>" alt="" /><p><b>Accesos habilitados</b> para personas con movilidad reducida</p></li>
			 	
			 	<li id="apropa"><img class=" wp-image-181" src="<?php echo esc_url(get_template_directory_uri()) . '/img/serveis/apropa.png'?>" alt="" /><p><b>Apropa Cultura:</b> Precios especiales para centros sociales que trabajen con personas con vulnerabilidad social o discapacidad a través del programa Apropa Cultura.</p></li>
			 	
			 	<li id="parking"><img class=" wp-image-181" src="<?php echo esc_url(get_template_directory_uri()) . '/img/serveis/parking.png'?>" alt="" /><p><b>1h de aparcamiento gratuito</b> en el Parking del Torrente Ballester<br>C / Virgen de Sales, 69. Hay que pedir el ticket en la taquilla del teatro.</p></li>
			 	
			 	<!-- <li id="bar"><img class=" wp-image-181" src="<?php echo esc_url(get_template_directory_uri()) . '/img/serveis/bar.png'?>" alt="" /><p><b>Servicio de bar y restaurante</b> todos los días de espectáculo</p></li> -->
				 <li id="bar"><img class=" wp-image-180" src="<?php echo esc_url(get_template_directory_uri()) . '/img/serveis/bar.png'?>" alt=""/><p><strong>Servicio de bar y restaurante</strong> todos los días de espectáculo <br><strong class="uk-text-small uk-font-underline" style="text-decoration:underline"> Servicio temporalmente fuera de servicio</strong></p></li>
			 	
			 	<li id="guarda-roba"><img class=" wp-image-180" src="<?php echo esc_url(get_template_directory_uri()) . '/img/serveis/guarda-roba.png'?>" alt="" /><p><b>Guardarropa</b> <br><strong class="uk-text-small uk-font-underline" style="text-decoration:underline"> Servicio temporalmente fuera de servicio siguiendo las indicaciones sanitarias por la Covid-19</strong></p></li>
			 	
			 	<li id="acollida"><img class=" wp-image-181" src="<?php echo esc_url(get_template_directory_uri()) . '/img/serveis/alzador.png'?>" alt="" /><p><b>Alzadores de butaca</b> para espectáculos familiares.</p></li>
			
			</ul>


		<p>Disponemos, además, de una larga tradición de actividades paralelas: masterclass, mesas redondas con los artistas, copa de vino al final del espectáculo, Photo-Call con los actores … ¡Pulsa sobre la imagen y bájate el programa con las próximas actividades de esta temporada! <br><strong class="uk-text-small uk-font-underline" style="text-decoration:underline"> Servicio temporalmente fuera de servicio siguiendo las indicaciones sanitarias por la Covid-19</strong></p>
				 


		</section>

		<section>
			<?php if ( have_posts()) : while ( have_posts() ) : the_post(); ?>
			
			<?php the_content(); ?>


		<?php endwhile; ?>

		<?php else : ?>

			<!-- article -->
			<article>

				<h2><?php esc_html_e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

			</article>
			<!-- /article -->

		<?php endif; ?>
		</section>
		
	</main>

<?php edit_post_link(); ?>

<?php get_footer(); ?>
